# Excel2Json

converts excel sheets abstractly into JSON format data to be used in further data analysis by different platforms

##How to Use

The converter works on command line, followed by the name of the worksheet that wishes to be converted.

The converted file will have the same name as the original file, with a JSON format.

example:

```

python3 JsonConverter.py example.xls

```

## Requisites 

The titles of the dictionaries linking to the JSON file, needs to be on the first row of the file
