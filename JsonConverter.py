import xlrd
from collections import OrderedDict
import simplejson as json
import sys



# open workbook
wb = xlrd.open_workbook(str(sys.argv[1]))
# select sheet from excel
sh = wb.sheet_by_index(0)

books_list = []
titles = []

#iterate through books list and add items into List
for rownum in range(0,sh.nrows):
    books = OrderedDict()
    row_values = sh.row_values(rownum)
    if rownum is 0:
        for title in range(0,len(row_values)):
            titles.append(row_values[title])
    else:
        for details in range(0,len(row_values)):
            books[str(titles[details])] = row_values[details]

        books_list.append(books)

# serialise the list into Json Format
books_json = json.dumps(books_list)

if ".xlsx" in str(sys.argv[1]):
    filename = str(sys.argv[1]).replace('.xlsx',"")
else:
    filename = str(sys.argv[1]).replace('.xls',"")


# write to the file
with open( filename + '.json','w') as writer:
    writer.write(books_json)
